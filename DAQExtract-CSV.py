from __future__ import with_statement
import sys
from PyQt5.QtWidgets import QWidget, QCheckBox, QLabel, QLineEdit, QApplication, QPushButton, QFileDialog, QListWidget, QListWidgetItem, QProgressBar, QMessageBox
from PyQt5.QtCore import Qt
import struct
import copy
import numpy as np
#import xlsxwriter
from scipy import io as sio
import os

#alphabetize list of variables
#load/save preferences
#timestamp
#list selection buttons


class MainWindow(QWidget):

	def __init__(self):
		super(MainWindow, self).__init__()
		
		self.initUI()
		
	def initUI(self):
		
		self.filelabel1 = QLabel('DAQ File:', self)
		self.filelabel1.move(10, 12)

		self.filebox = QLineEdit(self)
		self.filebox.move(100, 10)
		self.filebox.setFixedWidth(300)
		###
		self.filebox.setText('/Users/Bryan/test.daq')
		
		self.browsebutton = QPushButton('Browse', self)
		self.browsebutton.move(408, 5)
		self.browsebutton.clicked.connect(self.openFileNameDialog)	 
		
		self.loadbutton = QPushButton('Load File', self)
		self.loadbutton.move(100, 40)
		self.loadbutton.clicked.connect(self.loadFile)
		
		self.matexport = QCheckBox('Also export MAT file (all variables)', self)
		self.matexport.move(195, 45)
		
		self.varlabel = QLabel('Variables Available for Export:', self)
		self.varlabel.move(10, 100)
		
		self.progress = QProgressBar(self)
		self.progress.move(25, 75)
		self.progress.setFixedWidth(450)
		self.progress.setRange(0, 100)
		
		self.varlist = QListWidget(self)
		self.varlist.move(25, 125)
		self.varlist.resize(450, 250)
		
		self.checkall = QPushButton('Check All', self)
		self.checkall.move(25, 380)
		self.checkall.clicked.connect(self.checkallF)
		
		self.uncheckall = QPushButton('Uncheck All', self)
		self.uncheckall.move(125, 380)
		self.uncheckall.clicked.connect(self.uncheckallF)
		
		
		self.uncheckall = QPushButton('Export Selected Variables', self)
		self.uncheckall.move(140, 420)
		self.uncheckall.clicked.connect(self.exportVar)
		
		self.filesaved = QLabel('Exported as:', self)
		self.filesaved.move(25, 460)
		self.filesaved.setFixedWidth(450)
		
		self.setGeometry(300, 300, 500, 500)
		self.setWindowTitle('DAQ Attack')	  
		self.show()
		
	def openFileNameDialog(self):	 
		filepath = QFileDialog.getOpenFileName(self, 'Open file','~',"DAQ files (*.daq)")
		
		if filepath:
			self.filebox.setText(filepath[0])
			
	def loadFile(self):
		global daqdata
		w = QWidget()
		try:
			filepath = self.filebox.text()
			daqdata = convert_daq(filepath,self.matexport.checkState(),'','')
		except:	
			result = QMessageBox.warning(w, 'Warning', 'Invalid file selection!', QMessageBox.Ok)
			return
		self.progress.setValue(5)
		
		self.progress.setValue(50)
		for item in range(0, len(list(daqdata['elemData']))):
			self.progress.setValue(50+(50*(item/len(list(daqdata['elemData'])))))
			self.addItem(list(daqdata['elemData'])[item])
		self.progress.setValue(100)

	def addItem(self, varName):
		item = QListWidgetItem(varName, self.varlist);
		item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
		item.setCheckState(Qt.Unchecked)
		
	def checkallF(self):
		for index in xrange(self.varlist.count()):
			self.varlist.item(index).setCheckState(Qt.Checked)
			
	def uncheckallF(self):
		for index in xrange(self.varlist.count()):
			self.varlist.item(index).setCheckState(Qt.Unchecked)
		
	def exportVar(self):
		w = QWidget()
		global daqdata
		varArray = []
		try:
			filepath = self.filebox.text()
			savepath = os.path.dirname(filepath) + os.path.abspath(filepath)[0]
			filename = os.path.basename(filepath).rsplit('.',1)[0]
			wbpath = savepath + filename + '.csv'
			exportList=[]
			for index in xrange(self.varlist.count()):
				if (self.varlist.item(index).checkState() == 2):
					exportList.append(index)
			w = QWidget()
			if (len(exportList) == 0):
				result = QMessageBox.warning(w, 'Warning', 'Nothing selected for export!', QMessageBox.Ok)
				return
			for element in exportList:
				varArray.append(daqdata['elemData'])[element])
			for element in exportList:
				varArray.append(list(daqdata['elemData'][self.varlist.item(element).text()].flatten()))
			print(varArray)
			numpy.savetxt(wbpath, varArrayTrans, delimiter=',')
			newLabel = 'Exported as: ' + str(wbpath)
			self.filesaved.setText(newLabel)
		except:
			result = QMessageBox.warning(w, 'Warning', 'Export Failed!', QMessageBox.Ok)
			return
		
def daq_meta(fid):
	daq={}
	daq['magic'] = fid.read(4).encode('hex')
	daq['title'] = fid.read(120).split('\x00')[0]
	daq['date'] = fid.read(27).split('\x00')[0]
	daq['subject'] = fid.read(128).split('\x00')[0]
	daq['run'] = fid.read(128).split('\x00')[0]
	daq['runinst'] = fid.read(129).split('\x00')[0]
	daq['numentries'] = struct.unpack('i',fid.read(4))[0]
	daq['frequency'] = struct.unpack('i',fid.read(4))[0]
	return daq

def init_header():
	header={}
	header['id']=[]
	header['numvalues']=[]
	header['name']=[]
	header['units']=[]
	header['rate']=[]
	header['type']=[]
	header['varrateflag']=[]
	header['bytes']=[]
	return header

def init_frame():
	frame={}
	frame['code']=[]
	frame['frame']=[]
	frame['count']=[]
	return frame

def init_data(header, elemlist = []):
	data={}
	if elemlist:
		for name in header['name']:
			if name in elemlist:
				data[name]=[]
	else:
		for name in header['name']:
			data[name]=[]
	return data

def append_header(fid, header):
	if not header['id']:
		header['id']=[0]
	else:
		header['id'].append(header['id'][-1]+1)
	numvalues=struct.unpack('i',fid.read(4))[0]
	header['numvalues'].append(numvalues)
	header['name'].append(fid.read(36).split('\x00')[0])
	header['units'].append(fid.read(16).split('\x00')[0])
	rate=struct.unpack('h',fid.read(2))[0]
	if rate == 65535:
		rate=-1
	header['rate'].append(rate)
	dummy=fid.read(2)
	type=chr(struct.unpack('i', fid.read(4))[0])
	header['type'].append(type)
	header['varrateflag'].append(struct.unpack('B', fid.read(1))[0])
	post1=fid.read(3)

def append_data(fid, header, frame, data, elemlist = []):
	id = struct.unpack('i', fid.read(4))[0]
	if header['varrateflag'][id]==1:
		numitems = struct.unpack('i', fid.read(4))[0]
	else:
		numitems = header['numvalues'][id]
	type = header['type'][id]
	if type == 'i' or type == 'f':
		size = 4
	elif type == 's':
		type = 'h'
		size = 2
	elif type == 'c':
		size = 1
	elif type == 'd':
		size = 8
	#bytes = numitems*size
	name = header['name'][id]
	dataframe=[struct.unpack(type, fid.read(size))[0] for i in range(numitems)]
#	 if 'ET_filtered_gaze_object_name' in name:
#		 pdb.set_trace()
	if not elemlist or name in elemlist:
		if not data[name]:
			data[name]=map(copy.copy, [[]]*numitems)
		for i in range(numitems):
			data[name][i].append(dataframe[i])
		if header['rate'][id]==-1:
			try:
				data[name+'_Frames'].append(frame['frame'][-1])
			except:
				data[name+'_Frames']=[frame['frame'][-1]]

def read_file(filename, elemfile = ''):
	if elemfile:
		elemlist = read_elemlist(elemfile)
	else:
		elemlist = []
	with open(filename,'rb') as f:
		daqdata={}
		daq=daq_meta(f)
		header = init_header()
		for i in range(daq['numentries']):
			append_header(f,header)
		frame = init_frame()
		data = init_data(header, elemlist)
		while True:
			try:
				frame['code'].append(struct.unpack('i',f.read(4))[0])
			except Exception, err:
				break;
			if frame['code'][-1]==-2:
				break
			frame['frame'].append(struct.unpack('i',f.read(4))[0])
			frame['count'].append(struct.unpack('i',f.read(4))[0])
			for j in range(frame['count'][-1]):
				try:
					append_data(f, header, frame, data, elemlist)
				except Exception, err:
					break;
		data['Frames'] = frame['frame']
		daqdata['daqInfo']=daq
		daqdata['elemInfo']=header
		daqdata['elemFrames']=frame
		daqdata['elemData']=data
		return daqdata

def convert_file(daqdata, filename, exportMat):
	for cell in daqdata['elemData']:
		daqdata['elemData'][cell]=np.array(daqdata['elemData'][cell],order='F').transpose()
	if (exportMat == 2):
		sio.savemat(filename,daqdata,long_field_names=True,oned_as='column')

def read_elemlist(filename):
	with open(filename,'r') as f:
		elems = f.read().split('\n')
	elems = [elem.rstrip() for elem in elems]
	return elems

def convert_daq(filename, exportMat, elemfile='', outname='', outpath=''):
	daqdata=read_file(filename,elemfile)
	if not outname:
		outname = os.path.splitext(filename)[0]+'.mat'
	if outpath:
		outname = os.path.join(outpath,outname)
	convert_file(daqdata, outname, exportMat)
	return daqdata



def main():
	
	app = QtGui.QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())


if __name__ == '__main__':
	
	app = QApplication(sys.argv)
	windows = MainWindow()
	sys.exit(app.exec_())