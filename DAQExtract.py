from __future__ import with_statement
import sys
from PyQt5.QtWidgets import QWidget, QCheckBox, QLabel, QLineEdit, QApplication, QPushButton, QFileDialog, QListWidget, QListWidgetItem, QProgressBar, QMessageBox
from PyQt5.QtCore import Qt
import struct
import copy
import numpy as np
import xlsxwriter
from scipy import io as sio
import os
import csv

class MainWindow(QWidget):

	def __init__(self):
		super(MainWindow, self).__init__()
		
		self.initUI()
		
	def initUI(self):
		
		self.browsebutton = QPushButton('Browse File', self)
		self.browsebutton.move(100, 10)
		self.browsebutton.setToolTip('For individual file use')
		self.browsebutton.clicked.connect(self.openFileNameDialog)
		
		
		self.dirbrowsebutton = QPushButton('Browse Directory', self)
		self.dirbrowsebutton.move(250, 10)
		self.dirbrowsebutton.setToolTip('For batch use')
		self.dirbrowsebutton.clicked.connect(self.openDirDialog)
		
		self.filelabel1 = QLabel('DAQ File/Directory:', self)
		self.filelabel1.move(10, 52)
		
		self.filebox = QLineEdit('Click Browse to Begin', self)
		self.filebox.move(130, 50)
		self.filebox.setFixedWidth(300)
		self.filebox.setReadOnly(True)
		self.filebox.setText('/Users/Bryan/test.daq')
		
		self.progressDiv = QProgressBar(self)
		self.progressDiv.move(5, 85)
		self.progressDiv.setFixedWidth(490)
		self.progressDiv.setFixedHeight(2)
		
		self.loadbutton = QPushButton('Load DAQ File(s)', self)
		self.loadbutton.move(25, 95)
		self.loadbutton.clicked.connect(self.loadFunction)
		
		self.matexport = QCheckBox('Also export MAT file(s) (contain all variables)', self)
		self.matexport.move(175, 100)
		
		self.progress = QProgressBar(self)
		self.progress.move(50, 130)
		self.progress.setFixedWidth(400)
		self.progress.setRange(0, 100)
		
		self.varlabel = QLabel('Variables Available for Export:', self)
		self.varlabel.move(10, 150)
		
		self.varlist = QListWidget(self)
		self.varlist.move(25, 175)
		self.varlist.resize(325, 250)
		
		self.selectonly = QPushButton('Show Only \n Selected', self)
		self.selectonly.move(375, 175)
		self.selectonly.clicked.connect(self.selectOnly)
		
		self.showall = QPushButton('Show All', self)
		self.showall.move(380, 225)
		self.showall.clicked.connect(self.showAll)
		
		self.loadpref = QPushButton('Load Saved \n Selections', self)
		self.loadpref.move(365, 340)
		self.loadpref.clicked.connect(self.loadPref)
		
		self.savepref = QPushButton('Save Selections', self)
		self.savepref.move(355, 390)
		self.savepref.clicked.connect(self.savePref)
		
		self.uncheckall = QPushButton('Export Selected Variables', self)
		self.uncheckall.move(140, 440)
		self.uncheckall.clicked.connect(self.exportFunction)
		
		self.filesaved = QLabel('', self)
		self.filesaved.move(5, 470)
		self.filesaved.setFixedWidth(450)
		
		self.setGeometry(300, 300, 500, 500)
		self.setWindowTitle('DAQ Attack')	  
		self.show()
	
	def loadPref(self):
		w = QWidget()
		if (self.varlist.count() == 0):
			result = QMessageBox.warning(w, 'Warning', 'Need to load DAQ file/directory first!', QMessageBox.Ok)
			return
		filepath = QFileDialog.getOpenFileName(self, 'Open file','~',"Variable pref files (*.pref)")
		
		if filepath:
			fileIn = open(filepath[0], 'r')
			linesIn = [line.rstrip() for line in fileIn]
			overallFlag = False
			for item in linesIn:
				foundFlag = False
				for index in xrange(self.varlist.count()):
					if (self.varlist.item(index).text() == item):
						self.varlist.item(index).setCheckState(Qt.Checked)
						foundFlag = True
				if (foundFlag == False):
					overallFlag = True
			if (overallFlag == True):
				result = QMessageBox.warning(w, 'Warning', 'Could not find all variables in the list that were present in the preference file.', QMessageBox.Ok)
				return
			self.varlist.sortItems(Qt.AscendingOrder)
		
	def savePref(self):
		w = QWidget()
		filepath = self.filebox.text()
		savepath = os.path.dirname(filepath) + os.path.abspath(filepath)[0]
		filename = os.path.basename(filepath).rsplit('.',1)[0]
		savefile = savepath + filename + '.pref'
		saveList = []
		for index in xrange(self.varlist.count()):
			if (self.varlist.item(index).checkState() == 2):
				saveList.append(self.varlist.item(index).text())
		if (len(saveList)==0):
			result = QMessageBox.warning(w, 'Warning', 'Nothing selected', QMessageBox.Ok)
			return
		fileOut = open(savefile, 'w')
		for item in xrange(len(saveList)):
			fileOut.write(saveList[item])
			fileOut.write('\n')
		fileOut.close()
	
	def showAll(self):
		for index in xrange(self.varlist.count()):
			self.varlist.item(index).setHidden(False)
		self.varlist.sortItems(Qt.AscendingOrder)
	
	def selectOnly(self):
		for index in xrange(self.varlist.count()):
			if (self.varlist.item(index).checkState() != 2):
				self.varlist.item(index).setHidden(True)
		self.varlist.sortItems(Qt.AscendingOrder)
		
	def openFileNameDialog(self):	 
		global fileBool
		filepath = QFileDialog.getOpenFileName(self, 'Open file','~',"DAQ files (*.daq)")
		if filepath:
			self.filebox.setText(filepath[0])
		fileBool = True
	
	def openDirDialog(self):	
		global fileBool 
		dirpath = QFileDialog.getExistingDirectory(self, 'Open directory','~',QFileDialog.ShowDirsOnly)
		if dirpath:
			self.filebox.setText(dirpath)
		fileBool = False
			
	def loadFunction(self):
		global fileBool
		if (fileBool):
			filepath = self.filebox.text()
			self.loadFile(filepath)
		else:
			dirpath = self.filebox.text()
			self.loadDir(dirpath)
			
	def loadDir(self, dirpath):
		w = QWidget()
		global dirlist
		dirListPre = []
		dirlistPre = os.listdir(dirpath)
		if (dirlistPre == []):
			result = QMessageBox.warning(w, 'Warning', 'No files in selected folder!', QMessageBox.Ok)
			return
		dirlist = []
		for index in xrange(len(dirlistPre)):
			dirlistPre[index] = dirlistPre[index].encode('ascii', 'ignore')
		for index in xrange(len(dirlistPre)):
			if (dirlistPre[index].endswith('.daq')):
					dirlist.append(dirlistPre[index])
		if (dirlistPre == []):
			result = QMessageBox.warning(w, 'Warning', 'No files in selected folder!', QMessageBox.Ok)
			return
		dirpath = self.filebox.text()
		if (self.matexport.checkState() == 2):
			for item in dirlist:
				fullpath =  dirpath + os.path.abspath(dirpath)[0] + item
				self.loadFile(fullpath)
		else:
			fullpath =  dirpath + os.path.abspath(dirpath)[0] + dirlist[0]
			self.loadFile(fullpath)
	
	def loadFile(self, filepath):
		global daqdata
		global maxRows
		w = QWidget()
		while (self.varlist.count() > 0):
			self.varlist.takeItem(0)
		try:
			daqdata = convert_daq(filepath,self.matexport.checkState(),'','')
		except:	
			result = QMessageBox.warning(w, 'Warning', 'Invalid file selection!', QMessageBox.Ok)
			return
		self.progress.setValue(5)
		maxRows = 1
		for item in range(0, len(list(daqdata['elemData']))):
			try:
				if (daqdata['elemData'][list(daqdata['elemData'])[item]].shape[0] >= maxRows):
					maxRows = daqdata['elemData'][list(daqdata['elemData'])[item]].shape[0]
			except:
				True
		self.progress.setValue(50)
		for item in range(0, len(list(daqdata['elemData']))):
			self.progress.setValue(50+(50*(item/len(list(daqdata['elemData'])))))
			try:
				if (daqdata['elemData'][list(daqdata['elemData'])[item]].shape[0] >= maxRows):
					self.addItem(list(daqdata['elemData'])[item])
			except:
				True
		self.varlist.sortItems(Qt.AscendingOrder)
		self.progress.setValue(100)

	def addItem(self, varName):
		item = QListWidgetItem(varName, self.varlist);
		item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
		item.setCheckState(Qt.Unchecked)
		
	def exportFunction(self):
		global dirlist
		global fileBool
		if (fileBool):
			filepath = self.filebox.text()
			savepath = os.path.dirname(filepath) + os.path.abspath(filepath)[0]
			filename = os.path.basename(filepath).rsplit('.',1)[0]
			wbpath = savepath + filename + '.xlsx'
			self.exportVar(wppath)
			newLabel = 'Exported as: ' + str(wppath)
			self.filesaved.setText(newLabel)
		else:
			dirpath = self.filebox.text()
			for item in dirlist:
				savepath =  dirpath + os.path.abspath(dirpath)[0]
				filename = item.rsplit('.',1)[0]
				fullpath = savepath + filename + '.xlsx'
				self.exportVar(fullpath)
				
	
	def exportVar(self, filepath):
		w = QWidget()
		global daqdata
		global maxRows
		try:
			exportList=[]
			workbook = xlsxwriter.Workbook(filepath)
			worksheet = workbook.add_worksheet()
		except:
			result = QMessageBox.warning(w, 'Warning', 'Invalid file selection!', QMessageBox.Ok)
			return
		for index in xrange(self.varlist.count()):
			if (self.varlist.item(index).checkState() == 2):
				exportList.append(self.varlist.item(index).text())
		w = QWidget()
		if (len(exportList) == 0):
			result = QMessageBox.warning(w, 'Warning', 'Nothing selected for export!', QMessageBox.Ok)
			return
		timeStampList = []
		for step in range(1, maxRows + 1):
			stamp = step * (1.0/60.0)
			timeStampList.append(stamp)
		worksheet.write(0, 0, 'Timestamp')
		worksheet.write_column(1, 0, timeStampList)
		worksheet
		col=1
		for element in exportList:
			row = 0
			worksheet.write(row, col, element)
			row = 1
			worksheet.write_column(row, col, daqdata['elemData'][element].flatten())
			col = col + 1
		workbook.close()
		
		
		
def daq_meta(fid):
	daq={}
	daq['magic'] = fid.read(4).encode('hex')
	daq['title'] = fid.read(120).split('\x00')[0]
	daq['date'] = fid.read(27).split('\x00')[0]
	daq['subject'] = fid.read(128).split('\x00')[0]
	daq['run'] = fid.read(128).split('\x00')[0]
	daq['runinst'] = fid.read(129).split('\x00')[0]
	daq['numentries'] = struct.unpack('i',fid.read(4))[0]
	daq['frequency'] = struct.unpack('i',fid.read(4))[0]
	return daq

def init_header():
	header={}
	header['id']=[]
	header['numvalues']=[]
	header['name']=[]
	header['units']=[]
	header['rate']=[]
	header['type']=[]
	header['varrateflag']=[]
	header['bytes']=[]
	return header

def init_frame():
	frame={}
	frame['code']=[]
	frame['frame']=[]
	frame['count']=[]
	return frame

def init_data(header, elemlist = []):
	data={}
	if elemlist:
		for name in header['name']:
			if name in elemlist:
				data[name]=[]
	else:
		for name in header['name']:
			data[name]=[]
	return data

def append_header(fid, header):
	if not header['id']:
		header['id']=[0]
	else:
		header['id'].append(header['id'][-1]+1)
	numvalues=struct.unpack('i',fid.read(4))[0]
	header['numvalues'].append(numvalues)
	header['name'].append(fid.read(36).split('\x00')[0])
	header['units'].append(fid.read(16).split('\x00')[0])
	rate=struct.unpack('h',fid.read(2))[0]
	if rate == 65535:
		rate=-1
	header['rate'].append(rate)
	dummy=fid.read(2)
	type=chr(struct.unpack('i', fid.read(4))[0])
	header['type'].append(type)
	header['varrateflag'].append(struct.unpack('B', fid.read(1))[0])
	post1=fid.read(3)

def append_data(fid, header, frame, data, elemlist = []):
	id = struct.unpack('i', fid.read(4))[0]
	if header['varrateflag'][id]==1:
		numitems = struct.unpack('i', fid.read(4))[0]
	else:
		numitems = header['numvalues'][id]
	type = header['type'][id]
	if type == 'i' or type == 'f':
		size = 4
	elif type == 's':
		type = 'h'
		size = 2
	elif type == 'c':
		size = 1
	elif type == 'd':
		size = 8
	#bytes = numitems*size
	name = header['name'][id]
	dataframe=[struct.unpack(type, fid.read(size))[0] for i in range(numitems)]
#	 if 'ET_filtered_gaze_object_name' in name:
#		 pdb.set_trace()
	if not elemlist or name in elemlist:
		if not data[name]:
			data[name]=map(copy.copy, [[]]*numitems)
		for i in range(numitems):
			data[name][i].append(dataframe[i])
		if header['rate'][id]==-1:
			try:
				data[name+'_Frames'].append(frame['frame'][-1])
			except:
				data[name+'_Frames']=[frame['frame'][-1]]

def read_file(filename, elemfile = ''):
	if elemfile:
		elemlist = read_elemlist(elemfile)
	else:
		elemlist = []
	with open(filename,'rb') as f:
		daqdata={}
		daq=daq_meta(f)
		header = init_header()
		for i in range(daq['numentries']):
			append_header(f,header)
		frame = init_frame()
		data = init_data(header, elemlist)
		while True:
			try:
				frame['code'].append(struct.unpack('i',f.read(4))[0])
			except Exception, err:
				break;
			if frame['code'][-1]==-2:
				break
			frame['frame'].append(struct.unpack('i',f.read(4))[0])
			frame['count'].append(struct.unpack('i',f.read(4))[0])
			for j in range(frame['count'][-1]):
				try:
					append_data(f, header, frame, data, elemlist)
				except Exception, err:
					break;
		data['Frames'] = frame['frame']
		daqdata['daqInfo']=daq
		daqdata['elemInfo']=header
		daqdata['elemFrames']=frame
		daqdata['elemData']=data
		return daqdata

def convert_file(daqdata, filename, exportMat):
	for cell in daqdata['elemData']:
		daqdata['elemData'][cell]=np.array(daqdata['elemData'][cell],order='F').transpose()
	if (exportMat == 2):
		sio.savemat(filename,daqdata,long_field_names=True,oned_as='column')

def read_elemlist(filename):
	with open(filename,'r') as f:
		elems = f.read().split('\n')
	elems = [elem.rstrip() for elem in elems]
	return elems

def convert_daq(filename, exportMat, elemfile='', outname='', outpath=''):
	daqdata=read_file(filename,elemfile)
	if not outname:
		outname = os.path.splitext(filename)[0]+'.mat'
	if outpath:
		outname = os.path.join(outpath,outname)
	convert_file(daqdata, outname, exportMat)
	return daqdata



def main():
	
	app = QtGui.QApplication(sys.argv)
	ex = Example()
	sys.exit(app.exec_())


if __name__ == '__main__':
	
	app = QApplication(sys.argv)
	windows = MainWindow()
	sys.exit(app.exec_())